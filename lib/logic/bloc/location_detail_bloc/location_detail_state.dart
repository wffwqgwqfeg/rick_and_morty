import 'package:rick_and_morty/data/models/location_model.dart';

abstract class LocationDetailState {}

class LocationDetailInitState extends LocationDetailState {}

class LocationDetailLoadedState extends LocationDetailState {
  LocationDetailLoadedState(this.location);

  final Location location;
}

class LocationDetailFailureState extends LocationDetailState {}
