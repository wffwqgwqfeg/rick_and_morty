abstract class LocationDetailEvent {}

class LocationDetailInitEvent extends LocationDetailEvent {}

class GetLocationDetailByIdEvent extends LocationDetailEvent {
  GetLocationDetailByIdEvent(this.id);

  final int id;
}
