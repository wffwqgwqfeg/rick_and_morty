
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/config.dart';
import 'package:rick_and_morty/data/models/location_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/location_repo.dart';
import 'package:rick_and_morty/logic/bloc/location_detail_bloc/location_detail_event.dart';
import 'package:rick_and_morty/logic/bloc/location_detail_bloc/location_detail_state.dart';

class LocationDetailBloc extends Bloc<LocationDetailEvent, LocationDetailState> {
  LocationDetailBloc() : super(LocationDetailInitState());

  final LocationRepository _repository = locator.get<LocationRepository>();

  @override
  Stream<LocationDetailState> mapEventToState(LocationDetailEvent event) async* {
    if (event is GetLocationDetailByIdEvent) {
      try {
        final Location _location = await _repository.getLocationById(event.id);
        yield LocationDetailLoadedState(_location);
      } on DioError catch (e) {
        logger.e(e);
        yield LocationDetailFailureState();
      }
    }
  }
}
