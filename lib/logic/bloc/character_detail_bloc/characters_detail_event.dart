abstract class CharacterDetailEvent {}

class CharactersDetailInitEvent extends CharacterDetailEvent {}

class GetCharacterDetailByIdEvent extends CharacterDetailEvent {
  GetCharacterDetailByIdEvent(this.id);

  final int id;
}
