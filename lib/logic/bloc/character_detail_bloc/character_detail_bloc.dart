import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/config.dart';
import 'package:rick_and_morty/data/models/character_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/character_repo.dart';
import 'package:rick_and_morty/logic/bloc/character_detail_bloc/characters_detail_event.dart';
import 'package:rick_and_morty/logic/bloc/character_detail_bloc/characters_detail_state.dart';

class CharacterDetailBloc extends Bloc<CharacterDetailEvent, CharacterDetailState> {
  CharacterDetailBloc() : super(CharactersDetailInitState());

  final CharacterRepository _repository = locator.get<CharacterRepository>();

  @override
  Stream<CharacterDetailState> mapEventToState(CharacterDetailEvent event) async* {
    if (event is GetCharacterDetailByIdEvent) {
      try {
        yield CharactersDetailInitState();
        final Character _character = await _repository.getCharacterById(event.id);
        yield CharacterDetailLoadedState(_character);
      } on DioError catch (e) {
        logger.e(e);
        yield CharacterDetailFailureState();
      }
      // ignore: avoid_catches_without_on_clauses
      catch (e) {
        logger.e(e);
        yield CharacterDetailFailureState();
      }
    }
  }
}
