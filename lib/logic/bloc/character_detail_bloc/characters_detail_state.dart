import 'package:rick_and_morty/data/models/character_model.dart';

abstract class CharacterDetailState {}

class CharactersDetailInitState extends CharacterDetailState {}

class CharacterDetailLoadedState extends CharacterDetailState {
  CharacterDetailLoadedState(this.character);

  final Character character;
}

class CharacterDetailFailureState extends CharacterDetailState {}
