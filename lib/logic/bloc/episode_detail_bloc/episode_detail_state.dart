import 'package:rick_and_morty/data/models/episode_model.dart';

abstract class EpisodeDetailState {}

class EpisodeDetailInitState extends EpisodeDetailState {}

class EpisodeDetailLoadedState extends EpisodeDetailState {
  EpisodeDetailLoadedState(this.episode);

  final Episode episode;
}

class EpisodeDetailFailureState extends EpisodeDetailState {}
