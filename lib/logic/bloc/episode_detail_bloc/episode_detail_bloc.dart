import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/config.dart';
import 'package:rick_and_morty/data/models/episode_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/episode_repo.dart';
import 'package:rick_and_morty/logic/bloc/episode_detail_bloc/episode_detail_event.dart';
import 'package:rick_and_morty/logic/bloc/episode_detail_bloc/episode_detail_state.dart';

class EpisodeDetailBloc extends Bloc<EpisodeDetailEvent, EpisodeDetailState> {
  EpisodeDetailBloc() : super(EpisodeDetailInitState());

  final EpisodeRepository _repository = locator.get<EpisodeRepository>();

  @override
  Stream<EpisodeDetailState> mapEventToState(EpisodeDetailEvent event) async* {
    if (event is GetEpisodeDetailByIdEvent) {
      try {
        final Episode _episode = await _repository.getEpisodeById(event.id);
        yield EpisodeDetailLoadedState(_episode);
      } on DioError catch (e) {
        logger.e(e);
        yield EpisodeDetailFailureState();
      }
    }
  }
}
