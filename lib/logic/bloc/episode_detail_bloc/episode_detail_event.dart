abstract class EpisodeDetailEvent {}

class EpisodeDetailInitEvent extends EpisodeDetailEvent {}

class GetEpisodeDetailByIdEvent extends EpisodeDetailEvent {
  GetEpisodeDetailByIdEvent(this.id);

  final int id;
}
