import 'package:flutter/material.dart';
import 'package:rick_and_morty/data/models/episode_model.dart';

@immutable
abstract class EpisodeState {}

class EpisodeInitial extends EpisodeState {}

class EpisodeLoaded extends EpisodeState {
  EpisodeLoaded(this.episodes);

  final List<Episode> episodes;
}

class EpisodeLoading extends EpisodeState {
  EpisodeLoading(this.oldEpisodes, {this.isFirstFetch = false});

  final List<Episode> oldEpisodes;
  final bool isFirstFetch;
}

class EpisodeError extends EpisodeState {}