import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/data/models/episode_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/episode_repo.dart';
import 'package:rick_and_morty/logic/episode_cubit/episode_state.dart';

import '../../config.dart';

class EpisodeCubit extends Cubit<EpisodeState> {
  EpisodeCubit() : super(EpisodeInitial());

  int _page = 1;
  final EpisodeRepository _repository = locator.get<EpisodeRepository>();

  void pullToRefreshEpisodes() {
    _page = 1;
    loadEpisodes();
  }

  void loadEpisodes() {
    try {
      if (state is EpisodeLoading) return;

      final currentState = state;

      var oldCharacters = <Episode>[];
      if (currentState is EpisodeLoaded) {
        oldCharacters = currentState.episodes;
      }

      emit(EpisodeLoading(oldCharacters, isFirstFetch: _page == 1));

      _repository.getAllEpisodesByPage(_page).then((newPosts) {
        _page++;

        final posts = (state as EpisodeLoading).oldEpisodes;
        posts.addAll(newPosts);
        emit(EpisodeLoaded(posts));
      });
    } on DioError catch (e) {
      logger.e(e);
      emit(EpisodeError());
    }
  }
}
