import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rick_and_morty/data/models/character_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/character_repo.dart';

import '../../config.dart';

part 'character_state.dart';

class CharacterCubit extends Cubit<CharacterState> {
  CharacterCubit() : super(CharacterInitial());

  int _page = 1;
  final CharacterRepository _repository = locator.get<CharacterRepository>();

  void pullToRefreshCharacters() async {
    _page = 1;

    await loadCharacters();
  }

  Future loadCharacters() async {
    var listCharacters;
    if (state is CharacterLoading) return;

    final currentState = state;

    var oldCharacters = <Character>[];
    if (currentState is CharacterLoaded) {
      oldCharacters = currentState.characters;
    }

    emit(CharacterLoading(oldCharacters, isFirstFetch: _page == 1));
    try {
      await _repository.getAllCharactersByPage(_page).then((newCharacters) {
        _page++;
        listCharacters = (state as CharacterLoading).oldCharacters;
        listCharacters.addAll(newCharacters);
        emit(CharacterLoaded(listCharacters));
      });
    } on DioError catch (e) {
      _page = 1;
      logger.e(e);
      emit(CharacterLoading(oldCharacters, error: true));
    }
  }
}
