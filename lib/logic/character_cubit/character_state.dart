part of 'character_cubit.dart';

@immutable
abstract class CharacterState {}

class CharacterInitial extends CharacterState {}

class CharacterLoaded extends CharacterState {
  CharacterLoaded(this.characters);

  final List<Character> characters;
}

class CharacterLoading extends CharacterState {
  CharacterLoading(this.oldCharacters, {this.isFirstFetch = false, this.error = false});

  final List<Character> oldCharacters;
  final bool isFirstFetch;
  final bool error;
}

class CharacterError extends CharacterState {
  CharacterError(this.characters, {this.error = false});

  final List<Character> characters;
  final bool error;
}
