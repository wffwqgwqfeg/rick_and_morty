import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/config.dart';
import 'package:rick_and_morty/data/models/location_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/location_repo.dart';

import 'location_state.dart';

class LocationCubit extends Cubit<LocationState> {
  LocationCubit() : super(LocationInitial());

  int _page = 1;
  final LocationRepository _repository = locator.get<LocationRepository>();

  void pullToRefreshLocations() {
    _page = 1;
    loadLocations();
  }

  void loadLocations() {
    try {
      if (state is LocationLoading) return;

      final currentState = state;

      var oldCharacters = <Location>[];
      if (currentState is LocationLoaded) {
        oldCharacters = currentState.locations;
      }

      emit(LocationLoading(oldCharacters, isFirstFetch: _page == 1));

      _repository.getAllLocationsByPage(_page).then((newPosts) {
        _page++;

        final posts = (state as LocationLoading).oldLocations;
        posts.addAll(newPosts);
        emit(LocationLoaded(posts));
      });
    } on DioError catch (e) {
      logger.e(e);
      emit(LocationError());
    }
  }
}
