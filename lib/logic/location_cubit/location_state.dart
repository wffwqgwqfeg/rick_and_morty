import 'package:flutter/material.dart';
import 'package:rick_and_morty/data/models/location_model.dart';

@immutable
abstract class LocationState {}

class LocationInitial extends LocationState {}

class LocationLoaded extends LocationState {
  LocationLoaded(this.locations);

  final List<Location> locations;
}

class LocationLoading extends LocationState {
  LocationLoading(this.oldLocations, {this.isFirstFetch = false});

  final List<Location> oldLocations;
  final bool isFirstFetch;
}

class LocationError extends LocationState {}
