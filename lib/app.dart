import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rick_and_morty/l10n/l10n.dart';
import 'package:rick_and_morty/logic/internet_cubit/internet_cubit.dart';
import 'package:rick_and_morty/ui/resources/theme.dart';
import 'package:rick_and_morty/ui/router/app_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'config.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: [
          BlocProvider<InternetCubit>(
            create: (context) => InternetCubit(connectivity: locator.get<Connectivity>()),
          ),
        ],
        child: MaterialApp(
          theme: appTheme,
          supportedLocales: L10n.all,
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          debugShowCheckedModeBanner: false,
          onGenerateRoute: AppRouter.onGenerateRoute,
          initialRoute: RouterPath.main,
        ),
      );
}
