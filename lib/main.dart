import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rick_and_morty/app.dart';
import 'package:rick_and_morty/inject_services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();
  await injectServices();
  runApp(const MyApp());
}

Future init() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}
