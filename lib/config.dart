import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';

final locator = GetIt.I;
final logger = Logger();
