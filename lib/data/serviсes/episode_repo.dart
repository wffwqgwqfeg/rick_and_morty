import 'package:rick_and_morty/data/models/episode_model.dart';

abstract class EpisodeRepository {
  Future<List<Episode>> getAllEpisodesByPage(int page);

  Future<Episode> getEpisodeById(int id);
}
