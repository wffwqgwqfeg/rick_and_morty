import 'package:rick_and_morty/data/models/location_model.dart';

abstract class LocationRepository {
  Future<List<Location>> getAllLocationsByPage(int page);

  Future<Location> getLocationById(int id);
}
