import 'package:dio/dio.dart';

const String apiHost = 'https://rickandmortyapi.com/api/';

Dio? client;

Dio assembleHttpClient() {
  client ??= Dio(
    BaseOptions(
      baseUrl: apiHost,
      headers: {'content-type': 'application/json'},
    ),
  );
  return client!;
}
