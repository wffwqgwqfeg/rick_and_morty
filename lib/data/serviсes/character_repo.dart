import 'package:rick_and_morty/data/models/character_model.dart';

abstract class CharacterRepository {
  Future<List<Character>> getAllCharacters();

  Future<List<Character>> getAllCharactersByPage(int page);

  Future<Character> getCharacterById(int id);
}
