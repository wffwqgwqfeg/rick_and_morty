import 'package:dio/dio.dart';
import 'package:rick_and_morty/config.dart';
import 'package:rick_and_morty/data/models/character_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/character_repo.dart';

class HttpCharacterRepository extends CharacterRepository {
  HttpCharacterRepository();

  final client = locator.get<Dio>();

  @override
  Future<List<Character>> getAllCharacters() async {
    final response = await client.get('/character');
    logger.v(response);
    final characters = List<Map<String, dynamic>>.from(response.data['results'])
        .map((json) => Character.fromJson(json))
        .toList();
    return characters;
  }

  @override
  Future<List<Character>> getAllCharactersByPage(int page) async {
    final response = await client.get('/character?page=$page');
    logger.v(response.data['results']);
    return List<Map<String, dynamic>>.from(response.data['results'])
        .map((json) => Character.fromJson(json))
        .toList();
  }

  @override
  Future<Character> getCharacterById(int id) async {
    final response = await client.get('/character/$id');
    logger.v(response.data['results']);
    return Character.fromJson(response.data);
  }
}
