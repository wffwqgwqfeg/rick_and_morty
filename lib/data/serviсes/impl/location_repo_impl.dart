import 'package:dio/dio.dart';
import 'package:rick_and_morty/data/models/location_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/location_repo.dart';

import '../../../config.dart';

class HttpLocationRepository extends LocationRepository {
  HttpLocationRepository();

  final client = locator.get<Dio>();

  @override
  Future<List<Location>> getAllLocationsByPage(int page) async {
    final response = await client.get('/location?page=$page');
    logger.v(response.data['results']);
    return List<Map<String, dynamic>>.from(response.data['results'])
        .map((json) => Location.fromJson(json))
        .toList();
  }

  @override
  Future<Location> getLocationById(int id) async {
    final response = await client.get('/location/$id');
    logger.v(response.data['results']);
    return Location.fromJson(response.data);
  }
}
