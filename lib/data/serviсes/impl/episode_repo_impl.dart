import 'package:dio/dio.dart';
import 'package:rick_and_morty/data/models/episode_model.dart';
import 'package:rick_and_morty/data/servi%D1%81es/episode_repo.dart';

import '../../../config.dart';

class HttpEpisodeRepository extends EpisodeRepository {
  HttpEpisodeRepository();

  final client = locator.get<Dio>();

  @override
  Future<List<Episode>> getAllEpisodesByPage(int page) async {
    final response = await client.get('/episode?page=$page');
    logger.v(response.data['results']);
    return List<Map<String, dynamic>>.from(response.data['results'])
        .map((json) => Episode.fromJson(json))
        .toList();
  }

  @override
  Future<Episode> getEpisodeById(int id) async {
    final response = await client.get('/episode/$id');
    logger.v(response.data['results']);
    return Episode.fromJson(response.data);
  }
}
