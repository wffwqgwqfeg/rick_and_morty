import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColors {
  ///ALL COLORS
  static const Color primary = Color(0xffffffff);
  static const Color black = Color(0xff000000);
  static const Color white = Color(0xffffffff);
  static const Color blue = Color(0xff0000ff);
}
