import 'package:flutter/material.dart';
import 'package:rick_and_morty/ui/resources/colors.dart';

class LocationItemCard extends StatelessWidget {
  const LocationItemCard({Key? key, required id, required name, required type, required onTap})
      : _id = id,
        _name = name,
        _type = type,
        _onTap = onTap,
        super(key: key);

  final int _id;
  final String _type;
  final String _name;
  final VoidCallback _onTap;

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: _onTap,
        child: SizedBox(
          height: 90,
          child: Card(
            color: Colors.grey.withOpacity(0.4),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Row(
                children: [
                  Text(_id.toString()),
                  const SizedBox(width: 12),
                  //  SizedBox(height: 50, child: Image.network(_image)),
                  const SizedBox(width: 26),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(_name),
                        Text(_type, style: const TextStyle(color: AppColors.blue)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
