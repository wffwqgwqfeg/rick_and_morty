import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/data/models/location_model.dart';
import 'package:rick_and_morty/logic/bloc/location_detail_bloc/location_detail_bloc.dart';
import 'package:rick_and_morty/logic/bloc/location_detail_bloc/location_detail_state.dart';

class DetailLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Location'),
        ),
        body: BlocBuilder<LocationDetailBloc, LocationDetailState>(builder: (context, state) {
          if (state is LocationDetailInitState) {
            return const Center(child: CircularProgressIndicator());
          }
          if (state is LocationDetailLoadedState) {
            return _buildBody(state.location);
          }
          return Container();
        }),
      );

  Widget _buildBody(Location location) => Padding(
        padding: const EdgeInsets.all(8),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _buildItem('id', location.id.toString()),
              _buildItem('name', location.name),
              _buildItem('type', location.type),
              _buildItem('dimension', location.dimension),
              _buildItem('residents total', location.residents.length.toString()),
              _getList(location.residents),
              _getRowList(location.residents),
            ],
          ),
        ),
      );

  Widget _getList(List list) => Wrap(
        children: [
          for (int i = 0; i < list.length; i++)
            Text(
              'житель ${i + 1}, ',
            ),
        ],
      );

  Widget _getRowList(List list) => Column(
        children: [
          for (int i = 0; i < list.length; i++)
            Align(alignment: Alignment.centerLeft, child: Text('житель ${i + 1}, ')),
        ],
      );

  Widget _buildItem(String name, String value) => Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  '$name : ',
                  textAlign: TextAlign.left,
                  style: const TextStyle(fontWeight: FontWeight.w700),
                )),
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                value != '' ? value : '-',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
        ],
      );
}
