import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rick_and_morty/data/models/location_model.dart';
import 'package:rick_and_morty/logic/bloc/location_detail_bloc/location_detail_bloc.dart';
import 'package:rick_and_morty/logic/bloc/location_detail_bloc/location_detail_event.dart';
import 'package:rick_and_morty/logic/location_cubit/location_cubit.dart';
import 'package:rick_and_morty/logic/location_cubit/location_state.dart';
import 'package:rick_and_morty/ui/router/app_router.dart';
import 'package:rick_and_morty/ui/screens/location/location_detail.dart';
import 'package:rick_and_morty/ui/screens/location/widgets/location_item_card.dart';

class LocationScreen extends StatelessWidget {
  final scrollController = ScrollController();

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<LocationCubit>(context).loadLocations();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    BlocProvider.of<LocationCubit>(context).loadLocations();
    return Scaffold(
      body: _buildList(),
    );
  }

  Widget _buildList() => BlocBuilder<LocationCubit, LocationState>(builder: (context, state) {
        if (state is LocationLoading && state.isFirstFetch) {
          return _loadingIndicator();
        }
        List<Location> list = [];
        bool isLoading = false;

        if (state is LocationError) {
          return const Center(child: Text('error'));
        }
        if (state is LocationLoading) {
          list = state.oldLocations;
          isLoading = true;
        } else if (state is LocationLoaded) {
          list = state.locations;
        }
        return RefreshIndicator(
          onRefresh: () async {
            BlocProvider.of<LocationCubit>(context).pullToRefreshLocations();
          },
          child: ListView.builder(
            controller: scrollController,
            itemBuilder: (context, index) {
              if (index < list.length) {
                return LocationItemCard(
                  id: list[index].id,
                  name: list[index].name,
                  type: list[index].type,
                  //todo detail locations
                  onTap: () => pushNewScreenWithRouteSettings(
                    context,
                    settings: RouteSettings(name: RouterPath.detail, arguments: list[index].url),
                    screen: BlocProvider(
                        create: (context) =>
                            LocationDetailBloc()..add(GetLocationDetailByIdEvent(list[index].id)),
                        child: DetailLocation()),
                    withNavBar: true,
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  ),
                );
              } else {
                Timer(const Duration(milliseconds: 30), () {
                  scrollController.jumpTo(scrollController.position.maxScrollExtent);
                });
                return _loadingIndicator();
              }
            },
            itemCount: list.length + (isLoading ? 1 : 0),
          ),
        );
      });

  Widget _loadingIndicator() => const Padding(
        padding: EdgeInsets.all(16),
        child: Center(child: CircularProgressIndicator()),
      );
}
