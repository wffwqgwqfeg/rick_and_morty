import 'package:flutter/material.dart';

class DisconnectedScreen extends StatelessWidget {
  const DisconnectedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const Scaffold(
        body: Center(child: Text('Disconnected')),
      );
}
