import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rick_and_morty/config.dart';
import 'package:rick_and_morty/data/models/character_model.dart';
import 'package:rick_and_morty/logic/bloc/character_detail_bloc/character_detail_bloc.dart';
import 'package:rick_and_morty/logic/bloc/character_detail_bloc/characters_detail_event.dart';
import 'package:rick_and_morty/logic/character_cubit/character_cubit.dart';
import 'package:rick_and_morty/ui/router/app_router.dart';
import 'package:rick_and_morty/ui/screens/character/detail_character.dart';
import 'package:rick_and_morty/ui/screens/character/widgets/character_item_card.dart';
import 'package:rick_and_morty/ui/screens/error_try_againe_screen.dart';

class CharacterScreen extends StatefulWidget {
  const CharacterScreen({
    Key? key,
  }) : super(key: key);

  @override
  _CharacterScreenState createState() => _CharacterScreenState();
}

class _CharacterScreenState extends State<CharacterScreen> {
  final _scrollController = ScrollController();

  bool _showError = false;

  void _setupScrollController(context) {
    _scrollController.addListener(() {
      if (_scrollController.position.atEdge) {
        if (_scrollController.position.pixels != 0) {
          BlocProvider.of<CharacterCubit>(context).loadCharacters();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _setupScrollController(context);
    BlocProvider.of<CharacterCubit>(context).loadCharacters();
    return Scaffold(
      body: _buildList(),
    );
  }

  Widget _buildList() => BlocBuilder<CharacterCubit, CharacterState>(builder: (context, state) {
        if (state is CharacterLoading && state.isFirstFetch) {
          return _loadingIndicator();
        }
        List<Character> list = [];
        bool isLoading = false;

        if (state is CharacterLoading) {
          logger.i('CharacterLoading state');
          list = state.oldCharacters;
          isLoading = true;
          _showError = state.error;
        } else if (state is CharacterLoaded) {
          logger.i('CharacterLoaded state');
          list = state.characters;
        }
        /*else if (state is CharacterError) {

          list = state.characters;
          _showError = state.error;
          isLoading = true;
        }*/
        return RefreshIndicator(
          onRefresh: () async {
            BlocProvider.of<CharacterCubit>(context).pullToRefreshCharacters();
          },
          child: ListView.builder(
            shrinkWrap: true,
            controller: _scrollController,
            itemBuilder: (context, index) {
              if (index < list.length) {
                return CharacterItemCard(
                  id: list[index].id,
                  name: list[index].name,
                  species: list[index].species,
                  image: list[index].image,
                  onTap: () => pushNewScreenWithRouteSettings(
                    context,
                    settings: RouteSettings(
                      name: RouterPath.detail,
                      arguments: ScreenArguments(id: list[index].id),
                    ),
                    screen: BlocProvider(
                        create: (context) =>
                            CharacterDetailBloc()..add(GetCharacterDetailByIdEvent(list[index].id)),
                        child: const DetailCharacter()),
                    withNavBar: true,
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  ),
                );
              } else {
                Timer(const Duration(milliseconds: 30), () {
                  _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
                });

                return _showError == true
                    ? Container(
                        height: 140,
                        child: ErrorTryAgainScreen(
                          onTap: () {
                            BlocProvider.of<CharacterCubit>(context).loadCharacters();
                            _showError = false;
                          },
                        ))
                    : _loadingIndicator();
              }
            },
            itemCount: list.length + (isLoading ? 1 : 0),
          ),
        );
      });
}

Widget _loadingIndicator() => const Padding(
      padding: EdgeInsets.all(16),
      child: Center(child: CircularProgressIndicator()),
    );
