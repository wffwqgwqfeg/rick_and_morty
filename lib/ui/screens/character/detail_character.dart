import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/data/models/character_model.dart';
import 'package:rick_and_morty/logic/bloc/character_detail_bloc/character_detail_bloc.dart';
import 'package:rick_and_morty/logic/bloc/character_detail_bloc/characters_detail_event.dart';
import 'package:rick_and_morty/logic/bloc/character_detail_bloc/characters_detail_state.dart';
import 'package:rick_and_morty/ui/resources/images.dart';
import 'package:rick_and_morty/ui/router/app_router.dart';
import 'package:rick_and_morty/ui/screens/error_try_againe_screen.dart';
import 'package:rick_and_morty/ui/screens/widgets/no_image_asset.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DetailCharacter extends StatelessWidget {
  const DetailCharacter({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Character'),
      ),
      body: BlocBuilder<CharacterDetailBloc, CharacterDetailState>(builder: (context, state) {
        if (state is CharactersDetailInitState) {
          return const Center(child: CircularProgressIndicator());
        }
        if (state is CharacterDetailLoadedState) {
          return _buildBody(state.character, context);
        }
        if (state is CharacterDetailFailureState) {
          return ErrorTryAgainScreen(
            onTap: () {
              BlocProvider.of<CharacterDetailBloc>(context)
                  .add(GetCharacterDetailByIdEvent(args.id));
            },
          );
        }
        return const Center();
      }),
    );
  }

  Widget _buildBody(Character character, BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: SingleChildScrollView(
          child: Column(children: [
            const SizedBox(height: 20),
            SizedBox(
                height: 200,
                width: double.infinity,
                child: FadeInImage.assetNetwork(
                  placeholder: AppImages.placeholder,
                  imageErrorBuilder: (context, exception, stackTrace) => const NoImagePlaceholder(),
                  image: character.image,
                )),
            const SizedBox(height: 20),
            _buildItem(AppLocalizations.of(context)!.id, character.id.toString()),
            _buildItem(AppLocalizations.of(context)!.name, character.name),
            _buildItem(AppLocalizations.of(context)!.status, character.status),
            _buildItem(AppLocalizations.of(context)!.species, character.species),
            _buildItem(AppLocalizations.of(context)!.type, character.type),
            _buildItem(AppLocalizations.of(context)!.gender, character.gender),
            _buildItem(AppLocalizations.of(context)!.originName, character.origin.name),
            _buildItem(AppLocalizations.of(context)!.locationName, character.location.name),
            _buildItem(
                AppLocalizations.of(context)!.episodesTotal, character.episode.length.toString()),
            _getList(character.episode),
            _getRowList(character.episode),
            const SizedBox(height: 20),
          ]),
        ),
      );

  Widget _getList(List list) => Wrap(
        children: [
          for (int i = 0; i < list.length; i++) Text('епизод ${i + 1}, '),
        ],
      );

  Widget _getRowList(List list) => Column(
        children: [
          for (int i = 0; i < list.length; i++)
            Align(alignment: Alignment.centerLeft, child: Text('епизод ${i + 1}, ')),
        ],
      );

  Widget _buildItem(String name, String value) => Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  '$name : ',
                  textAlign: TextAlign.left,
                  style: const TextStyle(fontWeight: FontWeight.w700),
                )),
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                value != '' ? value : '-',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
        ],
      );
}
