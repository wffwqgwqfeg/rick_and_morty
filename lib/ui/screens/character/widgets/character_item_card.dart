import 'package:flutter/material.dart';
import 'package:rick_and_morty/ui/resources/colors.dart';
import 'package:rick_and_morty/ui/resources/images.dart';
import 'package:rick_and_morty/ui/screens/widgets/no_image_asset.dart';

class CharacterItemCard extends StatelessWidget {
  const CharacterItemCard(
      {Key? key, required id, required name, required species, required image, required onTap})
      : _id = id,
        _name = name,
        _species = species,
        _image = image,
        _onTap = onTap,
        super(key: key);

  final int _id;
  final String _species;
  final String _name;
  final String _image;
  final VoidCallback _onTap;

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: _onTap,
        child: SizedBox(
          height: 90,
          child: Card(
            color: Colors.grey.withOpacity(0.4),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Row(
                children: [
                  SizedBox(width: 30, child: Center(child: Text(_id.toString()))),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: SizedBox(
                      height: 60,
                      child: ClipOval(
                          child: FadeInImage.assetNetwork(
                        placeholder: AppImages.placeholder,
                        imageErrorBuilder: (context, exception, stackTrace) =>
                            const NoImagePlaceholder(),
                        image: _image,
                      )),
                    ),
                  ),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          _name,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Text(_species, style: const TextStyle(color: AppColors.blue))),
                    ],
                  )),
                ],
              ),
            ),
          ),
        ),
      );
}
