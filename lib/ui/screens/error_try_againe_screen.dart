import 'package:flutter/material.dart';

class ErrorTryAgainScreen extends StatelessWidget {
  const ErrorTryAgainScreen({Key? key, required onTap})
      : _onTap = onTap,
        super(key: key);

  final VoidCallback _onTap;

  @override
  Widget build(BuildContext context) => Column(
        children: [
          const Expanded(child: Center(child: Text('упс, произошла ошибка'))),
          ElevatedButton(onPressed: _onTap, child: const Text('TRY AGAIN')),
          const SizedBox(height: 50),
        ],
      );
}
