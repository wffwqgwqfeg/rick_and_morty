import 'package:flutter/material.dart';
import 'package:rick_and_morty/ui/resources/colors.dart';

class EpisodeItemCard extends StatelessWidget {
  const EpisodeItemCard({Key? key, required id, required name, required episode, required onTap})
      : _id = id,
        _name = name,
        _episode = episode,
        _onTap = onTap,
        super(key: key);

  final int _id;
  final String _episode;
  final String _name;
  final VoidCallback _onTap;

  @override
  Widget build(BuildContext context) => InkWell(
    onTap: _onTap,
    child: SizedBox(
      height: 90,
      child: Card(
        color: Colors.grey.withOpacity(0.4),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              Text(_id.toString()),
              const SizedBox(width: 12),
              //  SizedBox(height: 50, child: Image.network(_image)),
              const SizedBox(width: 26),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(_name),
                    Text(_episode, style: const TextStyle(color: AppColors.blue)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
