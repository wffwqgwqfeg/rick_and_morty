import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/data/models/episode_model.dart';
import 'package:rick_and_morty/logic/bloc/episode_detail_bloc/episode_detail_bloc.dart';
import 'package:rick_and_morty/logic/bloc/episode_detail_bloc/episode_detail_state.dart';

class EpisodeDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Episode'),
        ),
        body: BlocBuilder<EpisodeDetailBloc, EpisodeDetailState>(builder: (context, state) {
          if (state is EpisodeDetailInitState) {
            return const Center(child: CircularProgressIndicator());
          }
          if (state is EpisodeDetailLoadedState) {
            return _buildBody(state.episode);
          }
          return Container();
        }),
      );

  Widget _buildBody(Episode episode) => Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            _buildItem('id', episode.id.toString()),
            _buildItem('name', episode.name),
            _buildItem('air_date', episode.airDate),
            _buildItem('episode', episode.episode),
            _buildItem('characters total', episode.characters.length.toString()),
            _getList(episode.characters)
          ],
        ),
      );

  Widget _getList(List list) => Wrap(
        children: [
          for (int i = 0; i < list.length; i++) Text('герой ${i + 1}, '),
        ],
      );

  Widget _buildItem(String name, String value) => Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  '$name : ',
                  textAlign: TextAlign.left,
                  style: const TextStyle(fontWeight: FontWeight.w700),
                )),
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                value != '' ? value : '-',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
        ],
      );
}
