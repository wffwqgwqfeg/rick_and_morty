import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rick_and_morty/data/models/episode_model.dart';
import 'package:rick_and_morty/logic/bloc/episode_detail_bloc/episode_detail_bloc.dart';
import 'package:rick_and_morty/logic/bloc/episode_detail_bloc/episode_detail_event.dart';
import 'package:rick_and_morty/logic/episode_cubit/episode_cubit.dart';
import 'package:rick_and_morty/logic/episode_cubit/episode_state.dart';
import 'package:rick_and_morty/ui/router/app_router.dart';
import 'package:rick_and_morty/ui/screens/episode/episode_detail.dart';
import 'package:rick_and_morty/ui/screens/episode/widgets/episode_item_card.dart';

class EpisodeScreen extends StatelessWidget {
  final scrollController = ScrollController();

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<EpisodeCubit>(context).loadEpisodes();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    BlocProvider.of<EpisodeCubit>(context).loadEpisodes();
    return Scaffold(
      body: _buildList(),
    );
  }

  Widget _buildList() => BlocBuilder<EpisodeCubit, EpisodeState>(builder: (context, state) {
        if (state is EpisodeLoading && state.isFirstFetch) {
          return _loadingIndicator();
        }
        List<Episode> list = [];
        bool isLoading = false;

        if (state is EpisodeError) {
          return const Center(child: Text('error'));
        }
        if (state is EpisodeLoading) {
          list = state.oldEpisodes;
          isLoading = true;
        } else if (state is EpisodeLoaded) {
          list = state.episodes;
        }
        return RefreshIndicator(
          onRefresh: () async {
            BlocProvider.of<EpisodeCubit>(context).pullToRefreshEpisodes();
          },
          child: ListView.builder(
            controller: scrollController,
            itemBuilder: (context, index) {
              if (index < list.length) {
                return EpisodeItemCard(
                  id: list[index].id,
                  name: list[index].name,
                  episode: list[index].episode,
                  onTap: () => pushNewScreenWithRouteSettings(
                    context,
                    settings: RouteSettings(name: RouterPath.detail, arguments: list[index].url),
                    screen: BlocProvider(
                        create: (context) =>
                            EpisodeDetailBloc()..add(GetEpisodeDetailByIdEvent(list[index].id)),
                        child: EpisodeDetail()),
                    withNavBar: true,
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  ),
                );
              } else {
                Timer(const Duration(milliseconds: 30), () {
                  scrollController.jumpTo(scrollController.position.maxScrollExtent);
                });
                return _loadingIndicator();
              }
            },
            itemCount: list.length + (isLoading ? 1 : 0),
          ),
        );
      });

  Widget _loadingIndicator() => const Padding(
        padding: EdgeInsets.all(16),
        child: Center(child: CircularProgressIndicator()),
      );
}
