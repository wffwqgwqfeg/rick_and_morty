import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rick_and_morty/logic/character_cubit/character_cubit.dart';
import 'package:rick_and_morty/logic/episode_cubit/episode_cubit.dart';
import 'package:rick_and_morty/logic/internet_cubit/internet_cubit.dart';
import 'package:rick_and_morty/logic/location_cubit/location_cubit.dart';
import 'package:rick_and_morty/ui/screens/character/character_screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:rick_and_morty/ui/screens/episode/episode_screen.dart';
import 'package:rick_and_morty/ui/screens/location/location_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  final PersistentTabController _controller = PersistentTabController(initialIndex: 0);

  @override
  Widget build(BuildContext context) => BlocBuilder<InternetCubit, InternetState>(
        builder: (context, state) {
          //todo uncomment to handle network connection
          /*  if (state is InternetDisconnected) {
            return DisconnectedScreen();
          }*/
          return Scaffold(
            body: PersistentTabView(
              context,
              controller: _controller,
              screens: _buildScreens(),
              items: _navBarsItems(context),
              handleAndroidBackButtonPress: true,
              resizeToAvoidBottomInset: true,
              backgroundColor: Colors.amber,
              decoration: NavBarDecoration(
                borderRadius: BorderRadius.circular(10.0),
                colorBehindNavBar: Colors.green,
              ),
              itemAnimationProperties: const ItemAnimationProperties(
                duration: Duration(milliseconds: 200),
                curve: Curves.ease,
              ),
              screenTransitionAnimation: const ScreenTransitionAnimation(
                animateTabTransition: true,
              ),
              navBarStyle: NavBarStyle.style6,
            ),
          );
        },
      );
}

List<Widget> _buildScreens() => [
      BlocProvider(create: (context) => CharacterCubit(), child: CharacterScreen()),
      BlocProvider(create: (context) => LocationCubit(), child: LocationScreen()),
      BlocProvider(create: (context) => EpisodeCubit(), child: EpisodeScreen()),
    ];

List<PersistentBottomNavBarItem> _navBarsItems(BuildContext context) => [
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.home),
        title: AppLocalizations.of(context)!.character,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.settings),
        title: AppLocalizations.of(context)!.location,
        // activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.grid),
        title: AppLocalizations.of(context)!.episode,
        // activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
