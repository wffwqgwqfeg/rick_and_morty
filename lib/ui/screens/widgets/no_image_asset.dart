import 'package:flutter/material.dart';
import 'package:rick_and_morty/ui/resources/images.dart';

class NoImagePlaceholder extends StatelessWidget {
  const NoImagePlaceholder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Image.asset(AppImages.placeholder);
}
