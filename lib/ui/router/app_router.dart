import 'package:flutter/material.dart';
import 'package:rick_and_morty/ui/screens/character/detail_character.dart';
import 'package:rick_and_morty/ui/screens/episode/episode_detail.dart';
import 'package:rick_and_morty/ui/screens/location/location_detail.dart';
import 'package:rick_and_morty/ui/screens/main_screen.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouterPath.main:
        return MaterialPageRoute(builder: (_) => const MainScreen(), settings: settings);
      case RouterPath.detail:
        return MaterialPageRoute(builder: (_) =>const DetailCharacter(), settings: settings);
      case RouterPath.location:
        return MaterialPageRoute(builder: (_) => DetailLocation(), settings: settings);
      case RouterPath.episode:
        return MaterialPageRoute(builder: (_) => EpisodeDetail(), settings: settings);
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => Scaffold(
            body: Center(child: Text('No route defined for ${settings.name}')),
          ),
        );
    }
  }
}

class RouterPath {
  static const String main = '/';
  static const String home = '/characters';
  static const String detail = '/detail';
  static const String location = '/location';
  static const String episode = '/episode';
}

class ScreenArguments {
  ScreenArguments({required this.id});

  final int id;
}
