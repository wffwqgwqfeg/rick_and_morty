import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:rick_and_morty/config.dart';
import 'package:rick_and_morty/data/servi%D1%81es/episode_repo.dart';
import 'package:rick_and_morty/data/servi%D1%81es/impl/location_repo_impl.dart';
import 'data/serviсes/assemble_http_client.dart';
import 'data/serviсes/character_repo.dart';
import 'data/serviсes/impl/character_repo_iml.dart';
import 'data/serviсes/impl/episode_repo_impl.dart';
import 'data/serviсes/location_repo.dart';

Future injectServices() async {
  locator.registerSingleton<Dio>(assembleHttpClient());
  // ignore: cascade_invocations
  locator.registerSingleton<CharacterRepository>(HttpCharacterRepository());
  locator.registerSingleton<LocationRepository>(HttpLocationRepository());
  locator.registerSingleton<EpisodeRepository>(HttpEpisodeRepository());
  // ignore: cascade_invocations
  locator.registerSingleton(Connectivity());
}
